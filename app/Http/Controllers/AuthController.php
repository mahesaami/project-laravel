<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
      //register.blade.php
      return view('register');
    }

    public function welcome_post(Request $request) {
      $namaDepan = $request['first'];
      $namaBelakang = $request['last'];

      return view('welcome', compact('namaDepan','namaBelakang'));
    }
}
