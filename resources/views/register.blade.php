<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title></title>
</head>

<body>
  <h1>Buat Account Baru!</h1>
  <h3>Sign Up Form</h3>
  <form class="" action="/welcome" method="post">

    @csrf
    <label for="first-name">First name:</label>
    <br>
    <input type="text" name="first" value="">
    <br> <br>
    <label for="last-name">Last name:</label>
    <br>
    <input type="text" name="last" value="">
    <br> <br>

    <p>Gender:</p>

    <div class="male">
      <input type="radio" name="male" value="male">
      <label for="male">Male</label>
    </div>
    <div class="female">
      <input type="radio" name="female" value="female">
      <label for="female">Female</label>
    </div>
    <div class="others">
      <input type="radio" name="others" value="others">
      <label for="others">Others</label>
    </div>
    <br><br>

    <label for="nationality">Nationality</label>
    <br><br>
    <select class="nationality" name="nationality">
      <option value="Indonesian">Indonesian</option>
      <option value="America">America</option>
      <option value="China">China</option>
    </select>
    <br><br>

    <label for="language">Language Spoken:</label>
    <br> <br>
    <input type="checkbox" name="bahasa" value="Bahasa Indonesia">
    <label for="bahasa">Bahasa Indonesia</label>
    <br>
    <input type="checkbox" name="english" value="English">
    <label for="english">English</label>
    <br>
    <input type="checkbox" name="others" value="Others">
    <label for="others">Others</label>
    <br><br>

    <label for="bio">Bio:</label>
    <br><br>
    <textarea name="bio" rows="8" cols="30"></textarea>
    <br><br>

    <input type="submit" name="sign up" value="Sign Up">
  </form>
</body>

</html>
